document.addEventListener( 'DOMContentLoaded', function () {
	document.querySelector( '#analise' ).addEventListener( 'click', onClick, false );

	function onClick()
	{
		document.querySelector( '#grade' ).innerHTML = 'Processing...';

		chrome.tabs.query( {currentWindow: true, active: true},
		                   function ( tabs ) {
			                   chrome.tabs.sendMessage( tabs[ 0 ].id, 'processAnswers', showScore )
		                   } );
	}

}, false );

function showScore( html )
{
	if ( html === undefined ) html = 'This is not the Google Form individual test page.';

	document.querySelector( '#grade' ).innerHTML = html;
}