chrome.runtime.onMessage.addListener( function ( msg, sender, sendResponse ) {
	if ( msg !== 'processAnswers' ) return false;

	sendResponse( process() );
} );

function process()
{
	let score = new Array( 81 );
	let answers = getAnswers();
	let scoreTable = getScoreTable();
	for ( let i = 1; i < 81; i++ ) {
		score[ i ] = getScore( scoreTable, answers, i );
	}

	score = summarizeScore( score );

	return createTable( score );
}

function summarizeScore( score )
{
	let rights = 0, wrongs = 0, acceptables = 0;

	for ( let i = 1; i < 81; i++ ) {
		if ( score[ i ] === 1 ) rights++;
		else
			if ( score[ i ] > 0 && score[ i ] < 1 ) acceptables++;
			else wrongs++;
	}

	let rightPorcentage = 0, wrongPorcentage = 0, acceptablePorcentage = 0;

	rightPorcentage = ( rights / 80 * 100 ).toFixed( 2 );
	wrongPorcentage = ( wrongs / 80 * 100 ).toFixed( 2 );
	acceptablePorcentage = ( acceptables / 80 * 100 ).toFixed( 2 );

	return {rights: rights, wrongs: wrongs, acceptables: acceptables, rightPorcentage: rightPorcentage, wrongPorcentage: wrongPorcentage, acceptablePorcentage: acceptablePorcentage};
}

function getAnswers()
{

	let options = document.querySelectorAll( 'label.freebirdFormviewerViewItemsRadioChoice.isChecked' );
	let answers = [];

	for ( let answer of options ) {
		if (
			answer.classList.contains( 'freebirdFormviewerViewItemsRadioIncorrect' ) ||
			answer.classList.contains( 'freebirdFormviewerViewItemsRadioCorrect' )
		) {
			let id = answer.closest( 'div.freebirdFormviewerViewItemsItemItem' ).querySelector( '.freebirdFormviewerViewItemsItemItemTitle' ).innerText.replace( /\D+/, '' ).replace( /[\s\*]+/, '' );
			answers[ id ] = answer.querySelector( 'div.isChecked' ).dataset.value.replaceAll( '’', "'" );
		}
	}

	return answers;
}

function createTable( score )
{

	let table = '<table cellpadding="4" cellspacing="0" border="1"><thead><tr><th>&nbsp;</th><th>Amount</th><th>Porcentage</th></tr></thead><tbody>';

	table += `<tr><td>Rights</td><td>${score.rights}</td><td>${score.rightPorcentage}</td></tr>`;
	table += `<tr><td>Wrongs</td><td>${score.wrongs}</td><td>${score.wrongPorcentage}</td></tr>`;
	table += `<tr><td>Acceptables</td><td>${score.acceptables}</td><td>${score.acceptablePorcentage}</td></tr>`;

	return table + '</tbody></table>';
}

function getScore( scoreTable, answers, number )
{
	try {
		return scoreTable[ number ][ answers[ number ] ];
	} catch ( e ) {
		debugger;
	}
}

function getScoreTable()
{
	let score = [];

	for ( let i = 1; i < 81; i++ ) {
		score[ i ] = {};
	}

	score[ 1 ][ "That's" ] = 0;
	score[ 1 ][ "He's" ] = 0;
	score[ 1 ][ "Is he" ] = 1;
	score[ 1 ][ "Are that" ] = 0;
	score[ 1 ][ "They are" ] = 0;
	score[ 2 ][ "she's" ] = 0;
	score[ 2 ][ "that's" ] = 0;
	score[ 2 ][ "it is" ] = 0;
	score[ 2 ][ "he is" ] = 1;
	score[ 2 ][ "brother" ] = 0;
	score[ 3 ][ "He's" ] = 0.5;
	score[ 3 ][ "She's" ] = 0;
	score[ 3 ][ "That is a" ] = 0;
	score[ 3 ][ "Is he" ] = 0;
	score[ 3 ][ "He's a" ] = 1;
	score[ 4 ][ "has" ] = 1;
	score[ 4 ][ "is" ] = 0.5;
	score[ 4 ][ "have" ] = 0;
	score[ 4 ][ "are" ] = 0;
	score[ 4 ][ "am" ] = 0;
	score[ 5 ][ "is" ] = 0;
	score[ 5 ][ "have" ] = 1;
	score[ 5 ][ "am" ] = 0;
	score[ 5 ][ "are" ] = 0.5;
	score[ 5 ][ "has" ] = 0;
	score[ 6 ][ "Does" ] = 1;
	score[ 6 ][ "Is" ] = 0.25;
	score[ 6 ][ "Do" ] = 0;
	score[ 6 ][ "Are" ] = 0;
	score[ 6 ][ "Can" ] = 0.5;
	score[ 7 ][ "he does" ] = 0;
	score[ 7 ][ "does he" ] = 0;
	score[ 7 ][ "he doesn't" ] = 1;
	score[ 7 ][ "doesn't he" ] = 0;
	score[ 7 ][ "he isn't" ] = 0.5;
	score[ 8 ][ "He hasn't" ] = 0;
	score[ 8 ][ "He does" ] = 0.5;
	score[ 8 ][ "He have" ] = 0;
	score[ 8 ][ "He has" ] = 1;
	score[ 8 ][ "He doesn't has" ] = 0;
	score[ 9 ][ "Does he know" ] = 0.5;
	score[ 9 ][ "Are you" ] = 0;
	score[ 9 ][ "Do you knows" ] = 0;
	score[ 9 ][ "Do he know" ] = 0;
	score[ 9 ][ "Do you know" ] = 1;
	score[ 10 ][ "study" ] = 0.5;
	score[ 10 ][ "is study" ] = 0;
	score[ 10 ][ "does study" ] = 0;
	score[ 10 ][ "studies" ] = 1;
	score[ 10 ][ "not study" ] = 0;
	score[ 11 ][ "Is" ] = 0;
	score[ 11 ][ "There's" ] = 0;
	score[ 11 ][ "There isn't" ] = 0;
	score[ 11 ][ "Is there" ] = 1;
	score[ 11 ][ "Is it" ] = 0.5;
	score[ 12 ][ "is" ] = 0.5;
	score[ 12 ][ "is having" ] = 0;
	score[ 12 ][ "is has" ] = 0;
	score[ 12 ][ "is have" ] = 0;
	score[ 12 ][ "has" ] = 1;
	score[ 13 ][ "have" ] = 0.5;
	score[ 13 ][ "are having" ] = 1;
	score[ 13 ][ "has" ] = 0;
	score[ 13 ][ "is have" ] = 0;
	score[ 13 ][ "are have" ] = 0;
	score[ 14 ][ "there is" ] = 0;
	score[ 14 ][ "there are" ] = 0;
	score[ 14 ][ "there aren't" ] = 1;
	score[ 14 ][ "there isn't" ] = 0.5;
	score[ 14 ][ "it isn't" ] = 0.5;
	score[ 15 ][ "I move" ] = 0.5;
	score[ 15 ][ "I am move" ] = 0;
	score[ 15 ][ "I'm move" ] = 0;
	score[ 15 ][ "I moves" ] = 0;
	score[ 15 ][ "I'm moving" ] = 1;
	score[ 16 ][ "Does" ] = 0.5;
	score[ 16 ][ "Did" ] = 1;
	score[ 16 ][ "Are" ] = 0;
	score[ 16 ][ "Do" ] = 0;
	score[ 16 ][ "Was" ] = 0;
	score[ 17 ][ "do" ] = 0;
	score[ 17 ][ "does" ] = 0.5;
	score[ 17 ][ "was" ] = 0;
	score[ 17 ][ "did" ] = 1;
	score[ 17 ][ "were" ] = 0;
	score[ 18 ][ "is" ] = 0.5;
	score[ 18 ][ "are" ] = 0;
	score[ 18 ][ "did" ] = 0;
	score[ 18 ][ "were" ] = 0;
	score[ 18 ][ "was" ] = 1;
	score[ 19 ][ "I like" ] = 0.5;
	score[ 19 ][ "I liked" ] = 0.5;
	score[ 19 ][ "I'd liked" ] = 0;
	score[ 19 ][ "I'd like" ] = 1;
	score[ 19 ][ "I was like" ] = 0;
	score[ 20 ][ "went" ] = 1;
	score[ 20 ][ "goed" ] = 0;
	score[ 20 ][ "liked" ] = 0.5;
	score[ 20 ][ "goes" ] = 0.5;
	score[ 20 ][ "go" ] = 0;
	score[ 21 ][ "any" ] = 1;
	score[ 21 ][ "a" ] = 0;
	score[ 21 ][ "an" ] = 0;
	score[ 21 ][ "no" ] = 0.5;
	score[ 21 ][ "the" ] = 0.5;
	score[ 22 ][ "them" ] = 0;
	score[ 22 ][ "an" ] = 0.5;
	score[ 22 ][ "any" ] = 0;
	score[ 22 ][ "some" ] = 1;
	score[ 22 ][ "they" ] = 0;
	score[ 23 ][ "should" ] = 0.5;
	score[ 23 ][ "don't" ] = 0.5;
	score[ 23 ][ "can" ] = 0.5;
	score[ 23 ][ "aren't" ] = 0;
	score[ 23 ][ "shouldn't" ] = 1;
	score[ 24 ][ "drinks" ] = 0;
	score[ 24 ][ "much" ] = 1;
	score[ 24 ][ "any" ] = 0;
	score[ 24 ][ "many" ] = 0.5;
	score[ 24 ][ "soft" ] = 0;
	score[ 25 ][ "don't" ] = 0.5;
	score[ 25 ][ "should" ] = 1;
	score[ 25 ][ "isn't" ] = 0;
	score[ 25 ][ "can't" ] = 0.5;
	score[ 25 ][ "shouldn't" ] = 0.5;
	score[ 26 ][ "you meeting" ] = 0;
	score[ 26 ][ "do you know" ] = 0.5;
	score[ 26 ][ "did you know" ] = 0.5;
	score[ 26 ][ "are you meeting" ] = 0.5;
	score[ 26 ][ "did you meet" ] = 1;
	score[ 27 ][ "We meet" ] = 0.5;
	score[ 27 ][ "We knew" ] = 0.5;
	score[ 27 ][ "We met" ] = 1;
	score[ 27 ][ "We meeting" ] = 0;
	score[ 27 ][ "We're meet" ] = 0;
	score[ 28 ][ "were you doing" ] = 1;
	score[ 28 ][ "did you do" ] = 0.5;
	score[ 28 ][ "do you do" ] = 0.5;
	score[ 28 ][ "was he doing" ] = 0.5;
	score[ 28 ][ "does he do" ] = 0.5;
	score[ 29 ][ "having" ] = 0;
	score[ 29 ][ "am having" ] = 0.5;
	score[ 29 ][ "was having" ] = 1;
	score[ 29 ][ "had" ] = 0.5;
	score[ 29 ][ "were having" ] = 0;
	score[ 30 ][ "was asking" ] = 0.5;
	score[ 30 ][ "asking" ] = 0;
	score[ 30 ][ "asked" ] = 1;
	score[ 30 ][ "were asking" ] = 0;
	score[ 30 ][ "ask" ] = 0;
	score[ 31 ][ "Did you meet" ] = 0.5;
	score[ 31 ][ "Have you ever met" ] = 1;
	score[ 31 ][ "Have you meet" ] = 0;
	score[ 31 ][ "Did ever you meet" ] = 0;
	score[ 31 ][ "Has you met" ] = 0;
	score[ 32 ][ "have met" ] = 0.5;
	score[ 32 ][ "know" ] = 0.5;
	score[ 32 ][ "met" ] = 1;
	score[ 32 ][ "have know" ] = 0;
	score[ 32 ][ "already met" ] = 0;
	score[ 33 ][ "last two years" ] = 0;
	score[ 33 ][ "tomorrow" ] = 0;
	score[ 33 ][ "two last years" ] = 0;
	score[ 33 ][ "two years ago" ] = 1;
	score[ 33 ][ "for two years" ] = 0.5;
	score[ 34 ][ "I know" ] = 0.5;
	score[ 34 ][ "I knew" ] = 0.5;
	score[ 34 ][ "I am knowing" ] = 0;
	score[ 34 ][ "I've knew" ] = 0;
	score[ 34 ][ "I've known" ] = 1;
	score[ 35 ][ "have gone" ] = 0.5;
	score[ 35 ][ "went" ] = 1;
	score[ 35 ][ "go" ] = 0.5;
	score[ 35 ][ "are going" ] = 0.5;
	score[ 35 ][ "have went" ] = 0;
	score[ 36 ][ "where's my dog" ] = 0.5;
	score[ 36 ][ "where my dog" ] = 0;
	score[ 36 ][ "what's my dog" ] = 0;
	score[ 36 ][ "where my dog is" ] = 1;
	score[ 36 ][ "what my dog is" ] = 0;
	score[ 37 ][ "Bob did" ] = 1;
	score[ 37 ][ "did Bob do" ] = 0.5;
	score[ 37 ][ "has Bob done" ] = 0.5;
	score[ 37 ][ "Bob does" ] = 0.5;
	score[ 37 ][ "Bob is done" ] = 0;
	score[ 38 ][ "have finished" ] = 0.5;
	score[ 38 ][ "needed finish" ] = 0;
	score[ 38 ][ "shouldn't finish" ] = 0.5;
	score[ 38 ][ "can finished" ] = 0;
	score[ 38 ][ "need to finish" ] = 1;
	score[ 39 ][ "have to" ] = 0.5;
	score[ 39 ][ "can" ] = 1;
	score[ 39 ][ "shouldn't" ] = 0.5;
	score[ 39 ][ "can't" ] = 0.5;
	score[ 39 ][ "need" ] = 0;
	score[ 40 ][ "should" ] = 0.5;
	score[ 40 ][ "don't have to" ] = 1;
	score[ 40 ][ "need to" ] = 0.5;
	score[ 40 ][ "can" ] = 0.5;
	score[ 40 ][ "can't" ] = 0.5;
	score[ 41 ][ "used to play" ] = 0.5;
	score[ 41 ][ "played" ] = 0;
	score[ 41 ][ "playing" ] = 0;
	score[ 41 ][ "use to play" ] = 1;
	score[ 41 ][ "plays" ] = 0;
	score[ 42 ][ "you did" ] = 0;
	score[ 42 ][ "no" ] = 0;
	score[ 42 ][ "you didn't" ] = 0;
	score[ 42 ][ "did you" ] = 0.5;
	score[ 42 ][ "didn't you" ] = 1;
	score[ 43 ][ "use to collect" ] = 0;
	score[ 43 ][ "collects" ] = 1;
	score[ 43 ][ "was collected" ] = 0;
	score[ 43 ][ "collect" ] = 0;
	score[ 43 ][ "is collecting" ] = 0.5;
	score[ 44 ][ "is he" ] = 0;
	score[ 44 ][ "he does" ] = 0;
	score[ 44 ][ "will he" ] = 0.5;
	score[ 44 ][ "won't he" ] = 1;
	score[ 44 ][ "doesn't he" ] = 0;
	score[ 45 ][ "isn't he" ] = 1;
	score[ 45 ][ "does he" ] = 0;
	score[ 45 ][ "no" ] = 0;
	score[ 45 ][ "is he" ] = 0.5;
	score[ 45 ][ "he is" ] = 0;
	score[ 46 ][ "have working" ] = 0;
	score[ 46 ][ "are working" ] = 0.5;
	score[ 46 ][ "have been working" ] = 1;
	score[ 46 ][ "worked" ] = 0.5;
	score[ 46 ][ "have worked" ] = 0.5;
	score[ 47 ][ "have work" ] = 0;
	score[ 47 ][ "haven't been working" ] = 1;
	score[ 47 ][ "work" ] = 0.5;
	score[ 47 ][ "didn't work" ] = 0.5;
	score[ 47 ][ "worked" ] = 0.5;
	score[ 48 ][ "both" ] = 0.5;
	score[ 48 ][ "some" ] = 0.5;
	score[ 48 ][ "either" ] = 0;
	score[ 48 ][ "any" ] = 0;
	score[ 48 ][ "neither" ] = 1;
	score[ 49 ][ "learn" ] = 1;
	score[ 49 ][ "learning" ] = 0;
	score[ 49 ][ "have learned" ] = 0.5;
	score[ 49 ][ "do learns" ] = 0;
	score[ 49 ][ "does learn" ] = 0;
	score[ 50 ][ "they start" ] = 0.5;
	score[ 50 ][ "they started" ] = 0.5;
	score[ 50 ][ "they'd start" ] = 0.5;
	score[ 50 ][ "they'll start" ] = 1;
	score[ 50 ][ "they're starting" ] = 0.5;
	score[ 51 ][ "that I come" ] = 0;
	score[ 51 ][ "me to come" ] = 1;
	score[ 51 ][ "that me come" ] = 0;
	score[ 51 ][ "to me come" ] = 0;
	score[ 51 ][ "come to me" ] = 0;
	score[ 52 ][ "is the Apex file" ] = 0;
	score[ 52 ][ "the Apex file is" ] = 0.5;
	score[ 52 ][ "was the Apex file" ] = 0;
	score[ 52 ][ "goes the Apex file" ] = 0;
	score[ 52 ][ "the Apex file was" ] = 1;
	score[ 53 ][ "I talk" ] = 0;
	score[ 53 ][ "I talked" ] = 0;
	score[ 53 ][ "if I had talking" ] = 0;
	score[ 53 ][ "if I had talked" ] = 1;
	score[ 53 ][ "if I had talk" ] = 0;
	score[ 54 ][ "you to go" ] = 1;
	score[ 54 ][ "to you go" ] = 0;
	score[ 54 ][ "you go" ] = 0;
	score[ 54 ][ "that you go" ] = 0;
	score[ 54 ][ "that go you" ] = 0;
	score[ 55 ][ "let you to stay" ] = 0;
	score[ 55 ][ "to let you stay" ] = 0;
	score[ 55 ][ "make you to stay" ] = 0;
	score[ 55 ][ "let you stay" ] = 1;
	score[ 55 ][ "let that you stay" ] = 0;
	score[ 56 ][ "is leaving" ] = 0;
	score[ 56 ][ "will leave" ] = 0;
	score[ 56 ][ "had left" ] = 1;
	score[ 56 ][ "left" ] = 0;
	score[ 56 ][ "has left" ] = 0;
	score[ 57 ][ "isn't going to be" ] = 0;
	score[ 57 ][ "will be" ] = 0;
	score[ 57 ][ "wouldn't be" ] = 1;
	score[ 57 ][ "would be" ] = 0.5;
	score[ 57 ][ "was" ] = 0.5;
	score[ 58 ][ "will have" ] = 0;
	score[ 58 ][ "had had" ] = 0;
	score[ 58 ][ "am having" ] = 0;
	score[ 58 ][ "can have" ] = 0;
	score[ 58 ][ "had" ] = 1;
	score[ 59 ][ "couldn't help" ] = 1;
	score[ 59 ][ "helped" ] = 0.5;
	score[ 59 ][ "can help" ] = 0;
	score[ 59 ][ "had helped" ] = 0.5;
	score[ 59 ][ "can't help" ] = 0;
	score[ 60 ][ "has started" ] = 0;
	score[ 60 ][ "should started" ] = 0;
	score[ 60 ][ "had started" ] = 1;
	score[ 60 ][ "starts" ] = 0;
	score[ 60 ][ "started" ] = 0.5;
	score[ 61 ][ "knew" ] = 0.5;
	score[ 61 ][ "will know" ] = 0;
	score[ 61 ][ "would knew" ] = 0;
	score[ 61 ][ "had known" ] = 1;
	score[ 61 ][ "have known" ] = 0;
	score[ 62 ][ "had prepared" ] = 0;
	score[ 62 ][ "prepared" ] = 0;
	score[ 62 ][ "will prepare" ] = 0.5;
	score[ 62 ][ "would've prepared" ] = 1;
	score[ 62 ][ "prepare" ] = 0;
	score[ 63 ][ "investigate" ] = 0.5;
	score[ 63 ][ "had investigated" ] = 1;
	score[ 63 ][ "would investigate" ] = 0;
	score[ 63 ][ "investigated" ] = 0.5;
	score[ 63 ][ "have investigated" ] = 0;
	score[ 64 ][ "prepared" ] = 0;
	score[ 64 ][ "could've prepared" ] = 1;
	score[ 64 ][ "had prepared" ] = 0;
	score[ 64 ][ "prepare" ] = 0;
	score[ 64 ][ "would prepare" ] = 0.5;
	score[ 65 ][ "might could" ] = 0;
	score[ 65 ][ "might have" ] = 0.5;
	score[ 65 ][ "had been able" ] = 0;
	score[ 65 ][ "might've been able" ] = 1;
	score[ 65 ][ "had been" ] = 0;
	score[ 66 ][ "is announcing" ] = 0;
	score[ 66 ][ "announced" ] = 0;
	score[ 66 ][ "has announced" ] = 0;
	score[ 66 ][ "is announced" ] = 0;
	score[ 66 ][ "was announced" ] = 1;
	score[ 67 ][ "did translated" ] = 0;
	score[ 67 ][ "have translated" ] = 0;
	score[ 67 ][ "was translated" ] = 0;
	score[ 67 ][ "have been translated" ] = 1;
	score[ 67 ][ "are translated" ] = 0.5;
	score[ 68 ][ "is awaited" ] = 0;
	score[ 68 ][ "was being awaited" ] = 1;
	score[ 68 ][ "is being awaited" ] = 0.5;
	score[ 68 ][ "is awaiting" ] = 0;
	score[ 68 ][ "has been awaiting" ] = 0;
	score[ 69 ][ "is interviewing" ] = 0;
	score[ 69 ][ "interviewed" ] = 0;
	score[ 69 ][ "is interviewed" ] = 0;
	score[ 69 ][ "was interviewing" ] = 0;
	score[ 69 ][ "was interviewed" ] = 1;
	score[ 70 ][ "turned" ] = 0;
	score[ 70 ][ "had been turned" ] = 1;
	score[ 70 ][ "has been turned" ] = 0;
	score[ 70 ][ "is turned" ] = 0;
	score[ 70 ][ "had turned" ] = 0;
	score[ 71 ][ "tell you" ] = 0.5;
	score[ 71 ][ "say you why" ] = 0;
	score[ 71 ][ "tell why" ] = 0;
	score[ 71 ][ "tell you why" ] = 1;
	score[ 71 ][ "tell to you" ] = 0;
	score[ 72 ][ "That worries me" ] = 0;
	score[ 72 ][ "I worry" ] = 0;
	score[ 72 ][ "That I worry" ] = 0;
	score[ 72 ][ "Why I worry" ] = 0;
	score[ 72 ][ "What worries me" ] = 1;
	score[ 73 ][ "was done" ] = 0;
	score[ 73 ][ "could be done" ] = 1;
	score[ 73 ][ "could have done" ] = 0;
	score[ 73 ][ "could do" ] = 0;
	score[ 73 ][ "has been done" ] = 0;
	score[ 74 ][ "should be taxed" ] = 1;
	score[ 74 ][ "should tax" ] = 0;
	score[ 74 ][ "shouldn't taxed" ] = 0;
	score[ 74 ][ "have been taxed" ] = 0;
	score[ 74 ][ "are taxed" ] = 0.5;
	score[ 75 ][ "are used" ] = 0.5;
	score[ 75 ][ "were use" ] = 0;
	score[ 75 ][ "shouldn't be used" ] = 1;
	score[ 75 ][ "used" ] = 0;
	score[ 75 ][ "should use" ] = 0;
	score[ 76 ][ "will reach" ] = 0;
	score[ 76 ][ "will have reached" ] = 1;
	score[ 76 ][ "will be reached" ] = 0;
	score[ 76 ][ "has reached" ] = 0;
	score[ 76 ][ "will reaching" ] = 0;
	score[ 77 ][ "will be living" ] = 1;
	score[ 77 ][ "will living" ] = 0;
	score[ 77 ][ "will have lived" ] = 0;
	score[ 77 ][ "live" ] = 0;
	score[ 77 ][ "will be lived" ] = 0;
	score[ 78 ][ "will be installed" ] = 0;
	score[ 78 ][ "will installed" ] = 0;
	score[ 78 ][ "have installed" ] = 0;
	score[ 78 ][ "will have installed" ] = 1;
	score[ 78 ][ "be installing" ] = 0;
	score[ 79 ][ "are disappearing" ] = 0;
	score[ 79 ][ "are disappeared" ] = 0;
	score[ 79 ][ "disappear" ] = 0;
	score[ 79 ][ "will have disappeared" ] = 1;
	score[ 79 ][ "have disappear" ] = 0;
	score[ 80 ][ "are visiting" ] = 0;
	score[ 80 ][ "will be visiting" ] = 1;
	score[ 80 ][ "have visited" ] = 0;
	score[ 80 ][ "visit" ] = 0;
	score[ 80 ][ "will be visited" ] = 0;

	return score;
}
